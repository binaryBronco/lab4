package Lab4Start;

    /**
     * EmployeeRules interface
     * @author  John Reagan
     * @version 1.0, 03/02/2021
     */

interface EmployeeRules {
    String getName();
    String getSalary();
}
