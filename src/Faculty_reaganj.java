package Lab4Start;

    /**
     * A Faculty class which extends the Employee class.
     * @author  John Reagan
     * @version 1.0, 03/02/2021
     */

class Faculty extends Lab4Start.Employee {
    // a private array that holds the courses taught by this faculty member
    private String[] courses;

    /**
     * Constructor
     * @param inName        The name of the faculty member.
     * @param inSalary      The salary of the faculty member.
     * @param inCourses     Courses taught by the faculty member.
     */
    public Faculty(String inName, int inSalary, String[] inCourses) {
        super(inName, inSalary);
        setCourses(inCourses);
    }

    /**
     * Default constructor
     */
    public Faculty() {
        super("No Name", 0);
        String[] empty = {""};
        setCourses(empty);
    }

    /**
     * The getName method overrides the superclass getName method. This version
     * precedes the name with the word "Professor"
     * @return name     The name of the faculty member.
     */
    @Override
    public String getName() { return "Professor " + super.getName(); }

    /**
     * Accessor getCourses returns a copy of the array of courses.
     * @return copy     A copy of the array of courses taught by this faculty.
     */
    public String[] getCourses() {
        String[] copy = new String[courses.length];
        for (int i = 0; i < courses.length; i++)
            copy[i] = courses[i];
        return copy;
    }

    /**
     * Accessor getCourseNames gets all of the course names as a string.
     * @return courseNames      A string of the courses taught by this faculty.
     */
    public String getCourseNames() {
        String courseNames = "";
        for (int j = 0; j < courses.length; j++)
            courseNames += courses[j] + ", ";
        return courseNames.substring(0, courseNames.length()-2);
    }

    /**
     * Mutator setCourses stores a copy of an input array to the field courses.
     * @param inCourses     An array of Strings to be set to the courses field.
     */
    public void setCourses(String[] inCourses) {
        courses = new String[inCourses.length];
        for (int k = 0; k < inCourses.length; k++)
            courses[k] = inCourses[k];
    }

    /**
     * Displays the state of the Faculty object.
     * @return The faculty parameters as a String.
     */
    public String toString() {
        return getName() + " has a salary of " + super.getSalary() +
                " and teaches the following courses: " + getCourseNames();
    }
}
