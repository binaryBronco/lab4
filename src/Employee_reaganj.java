package Lab4Start;

    /**
     * An Employee class which implements the interface of EmployeeRules.
     * @author  John Reagan
     * @version 1.0, 03/02/2021
     */

class Employee implements Lab4Start.EmployeeRules {
    private String name;    // the name of the employee
    private int salary;     // the salary of the employee

    /**
     * Constructor
     * @param inName    The name of the employee.
     * @param inSalary  The salary of the employee.
     */
    public Employee(String inName, int inSalary) {
        // this is the constructor of Employee with args
        name = inName;
        salary = inSalary;
    }

    /**
     * Default constructor
     */
    public Employee() { this("NO NAME", 0); }

    /**
     * Accessor getName returns the name of the employee.
     * @return name     The name of the employee.
     */
    public String getName() { return name; }

    /**
     * Accessor getSalary returns the salary of the employee.
     * @return salary   The salary of the employee.
     */
    public String getSalary() { return String.format("$%,d", salary); }

    /**
     * Displays the state of the Employee object.
     * @return The employee parameters as a String.
     */
    public String toString() {
        return "Employee " + getName() + " has a salary of " + getSalary() +".";
    }
}
